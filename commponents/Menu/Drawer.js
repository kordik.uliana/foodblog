import * as React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';
import Main from '../main';
import Feed from './Feed';
import ShoppingCart from './ShoppingCart';
import { Feather } from '@expo/vector-icons';
import { AntDesign } from '@expo/vector-icons';
import SelectedDish from './Account/SelectedDish'


const Drawer = createDrawerNavigator();

export default function MyDrawer() {
    return (
        <Drawer.Navigator >
            <Drawer.Screen name="Main" component={Main} options={{ headerTitle: '', headerShown: false }} />
            <Drawer.Screen name="Feed" component={Feed} />
            <Drawer.Screen name="ShoppingCart" component={ShoppingCart} options={{headerTitle: '', headerShown: false,
                drawerIcon: ({ size, color}) => (
                    <Feather name="shopping-cart" size={size} color={color} />
                )
            }} />
             <Drawer.Screen name="SelectedDish" component={SelectedDish} options={{headerTitle: '', headerShown: false,
                drawerIcon: ({ size, color}) => (
                    <AntDesign name="hearto" size={size} color={color} />
                )
            }} />
        </Drawer.Navigator>
    );
}