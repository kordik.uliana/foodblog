import React, { useState } from 'react';
import { View, StyleSheet, TouchableOpacity, Image } from 'react-native';
import Input from '../Header/Input';

export default function CustomHeader({ navigation, onSearch, onCategorySelected }) {//принимаем пропс с содержимым 

  return (
    <View style={styles.headerContainer}>

      <TouchableOpacity onPress={() => navigation.openDrawer()} style={styles.drawerButton}>
        <Image source={{ uri: 'https://cdn-icons-png.flaticon.com/128/12786/12786863.png' }} style={styles.image} />
      </TouchableOpacity>

      {/* передаем пропс */}
      <Input onSearch={onSearch} onCategorySelected={onCategorySelected}/>


      <TouchableOpacity onPress={() => navigation.navigate('Account')} style={styles.accountButton}>
        {/* Иконка для учетной записи */}
        <Image source={{ uri: 'https://cdn-icons-png.flaticon.com/128/9327/9327871.png' }} style={styles.image} />
      </TouchableOpacity>

    </View>
  );
}

const styles = StyleSheet.create({
  headerContainer: {
    // backgroundColor: 'green',
    marginBottom: 5,
    marginRight: 20,
    marginLeft: 20,
    flexDirection: 'row',
    marginTop: 50,
    height: 35,
    justifyContent: 'space-around',
    alignItems: 'flex-start',

  },
  drawerButton: {
    position: 'absolute',
    left: 4
  },

  input: {
    flex: 1, // Занимает все доступное пространство внутри контейнера inputContainer


  },
  accountButton: {
    position: 'absolute',
    right: 4
  },
  image: {
    width: 30,
    height: 30,
  },
});