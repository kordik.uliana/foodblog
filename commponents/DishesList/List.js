import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, FlatList, Image, } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { sliderData } from '../data'
import { LogBox } from 'react-native';


LogBox.ignoreLogs([
  'Non-serializable values were found in the navigation state',
]);


const BlockDish = ({ navigation, selectedCategory, searchText, addDish }) => {

    // console.log("AddDish in list.js:", addDish, );
    const filteredDishes = sliderData.filter(dish => {
        const isInCategory = dish.category === selectedCategory;//вывод информации по категории
        const isInSearch = !searchText ||
            dish.name.toLowerCase().includes(searchText.toLowerCase())
            || dish.full.toLowerCase().includes(searchText.toLowerCase())
            || dish.review.toLowerCase().includes(searchText.toLowerCase());//вывод ифнормации по имени   / .toLowerCase(): Этот метод приводит все символы в строке к нижнему регистру. / includes(searchText.toLowerCase()): Этот метод проверяет, содержится ли подстрока searchText, преобразованная к нижнему регистру, в строке dish.name 
        return isInCategory && isInSearch;
    });
    
    return (
        < FlatList
            data={filteredDishes}
            numColumns={2} // Устанавливаем количество колонок в два
            renderItem={({ item}) => ( //  рендерид каждый элемент 
                <>
                    <View style={styles.item}>
                        <TouchableOpacity onPress={() => navigation.navigate('FullInfo', { ...item, addDish})} >
                            <Image source={{ uri: item.img }}
                                style={{ width: '93%', height: 170, borderRadius: 20 }} />
                            <Text style={styles.name}>{item.name}</Text>
                            <Ionicons name='star' size={10} color='black' style={styles.iconStar}> <Text>{item.review}</Text></Ionicons>
                        </TouchableOpacity>
                        <View style={styles.tagsContainer}>
                            {item.tags.map((tag, index) => (
                                <TouchableOpacity key={index} onPress={() => navigation.navigate('Tags',{ ...item, tag  })}>
                                    <Text style={styles.tag}>{tag}</Text>
                                </TouchableOpacity>
                            ))}
                        </View>
                    </View>
                </>
            )}
            keyExtractor={item => item.key.toString()}
        />

    );
}
export default BlockDish;

const styles = StyleSheet.create({
    tagsContainer: {
        flexDirection: 'row', // Отображаем теги в строку
        flexWrap: 'wrap', // Позволяет переносить теги на новую строку, если они не помещаются в ширину экрана
        marginTop: 1, // Добавляем отступ сверху
    },
    tag: {
        fontSize: 10,
        paddingHorizontal: 3, // Добавляем немного отступа слева и справа от текста тега
       
        borderRadius: 5, // Делаем закругленные углы для тегов
        marginRight: 4, // Добавляем немного отступа между тегами
      
    },
    iconStar: {
        marginTop: -20,
        opacity: 0.7
    },
    name: {
        marginTop: 5,
        width: 160,
        fontWeight: "500",
        color: 'black',
        fontSize: 15,
        marginBottom: 20,
    },
    item: {
        flex: 1,
        width: '100%',
        marginBottom: 30,

    },
    header: {
        marginBottom: 30
    },
    title: {
        fontFamily: 'mono-bold',
        fontSize: 22,
        textAlign: 'center',
        marginTop: 20,
        color: '#474747'
    },
    anons: {
        fontFamily: 'mono-light',
        fontSize: 16,
        alignContent: 'center',
        marginTop: 5,
        color: '#474747'
    },
    iconAdd: {
        textAlign: 'center', marginBottom: 15,
    },
    iconClose: {
        textAlign: 'center', marginBottom: 15, marginTop: 17
    },

});
